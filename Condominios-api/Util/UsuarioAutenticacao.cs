﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class UsuarioAutenticacao
{
    public string Login { get; set; }
    public string Senha { get; set; }
    public bool IsProvedorMaste { get; set; }
}
