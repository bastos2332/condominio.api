﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class CAD232_ANDAMENTO_OCORRENCIA
{

    #region PROPS
    public int IdAndamentoOcorrencia { get; set; }
    public int IdSolicitacaoOcorrencia { get; set; }
    public int IdUsuarioAndamento { get; set; }
    public string DescricaoAndamento { get; set; }
    public DateTime DataAndamento { get; set; }
    public int IdStatusOcorrencia { get; set; }
    public int IdCliente { get; set; }
    public string Imagem { get; set; }
    #endregion

    #region METHODS
    public List<CAD232_ANDAMENTO_OCORRENCIA> List_CAd231(int idSolicitacaoOcorrencia)
    {
        using (var conexaoBD = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<CAD232_ANDAMENTO_OCORRENCIA>)conexaoBD.Query<CAD232_ANDAMENTO_OCORRENCIA>("usp_CAD232_ANDAMENTO_SOLICITACAO_OCORRENCIA_List_CAD231",
                                            new
                                            {
                                                @idSolicitacaoOcorrencia = idSolicitacaoOcorrencia,
                                            },
                                                commandType: CommandType.StoredProcedure);

        }

    }
    #endregion
}






