﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
public class CAD100_PROVEDOR
{
    #region PROPS   
    public int IdProvedor { get; set; }
    public string NomeProvedor { get; set; }
    public string NomeFantasia { get; set; }
    public bool IsProvedorMaster { get; set; }
    #endregion

    #region METODOS
    public List<CAD100_PROVEDOR> Get(int idProvedor)
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<CAD100_PROVEDOR>)cnx.Query<CAD100_PROVEDOR>("usp_CAD100_PROVEDOR_select", new { @idProvedor = idProvedor }, commandType: System.Data.CommandType.StoredProcedure);
        }
    }
    #endregion



}
