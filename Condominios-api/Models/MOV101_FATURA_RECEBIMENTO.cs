﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;

    public class MOV101_FATURA_RECEBIMENTO
    {


        public int IdRecebimento { get; set; }
    public int IdCliente { get; set; }
    public int IdFatura { get; set; }
        public DateTime DataRecebimento { get; set; }
        public decimal ValorCobrado { get; set; }


        public List<MOV101_FATURA_RECEBIMENTO> List()
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                return (List<MOV101_FATURA_RECEBIMENTO>)cnx.Query<MOV101_FATURA_RECEBIMENTO>("usp_MOV101_FATURA_List_CAD200_CLIENTE", new {@idCliente = this.IdCliente },commandType: CommandType.StoredProcedure);
            }
        }

    }
