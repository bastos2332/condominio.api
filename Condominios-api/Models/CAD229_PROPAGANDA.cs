﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class CAD229_PROPAGANDA
{
    #region PROPS
    public int IdPropaganda { get; set; }
    public int IdProvedor { get; set; }
    public int IdUsuario { get; set; }
    public string TituloPropaganda { get; set; }
    public string NomeArquivo { get; set; }
    public string DescricaoPropaganda { get; set; }
    #endregion

    #region METHODS
    public List<CAD229_PROPAGANDA> Get_CAD100(int idProvedor)
    {
        using (var conexaoBD = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<CAD229_PROPAGANDA>)conexaoBD.Query<CAD229_PROPAGANDA>("usp_CAD229_PROPAGANDA_List_CAD100",
                                            new
                                            {
                                                @idProvedor = idProvedor,
                                            },
                                                commandType: CommandType.StoredProcedure);

        }

    }
    #endregion
}
