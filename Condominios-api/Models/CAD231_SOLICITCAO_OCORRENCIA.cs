﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;


public class CAD231_SOLICITCAO_OCORRENCIA
{
    #region PROPS
    public int IdSolicitacaoOCorrencia { get; set; }
    public int IdProvedor { get; set; }
    public int IdUsuarioCadastro { get; set; }
    public int IdTipoSolicitacaoOcorrencia { get; set; }
    public string DescricaoSolicitacao { get; set; }
    public DateTime DataCadastroSolicitacao { get; set; }
    public int IdStatusOcorrencia { get; set; }
    public int IdCliente { get; set; }
    #endregion

    #region METHODS
    public List<CAD231_SOLICITCAO_OCORRENCIA> Get()
    {
        using (SqlConnection conexao = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<CAD231_SOLICITCAO_OCORRENCIA>)conexao.Query<CAD231_SOLICITCAO_OCORRENCIA>("usp_CAD231_SOLICITACAO_OCORRENCIA_List", commandType: CommandType.StoredProcedure);
        }
    }

    public List<CAD231_SOLICITCAO_OCORRENCIA> Get_CAD200(int idCliente)
    {
        using (SqlConnection conexao = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<CAD231_SOLICITCAO_OCORRENCIA>)conexao.Query<CAD231_SOLICITCAO_OCORRENCIA>("usp_CAD231_SOLICITACAO_OCORRENCIA_List_CAD200", new { @idCliente = idCliente }, commandType: CommandType.StoredProcedure);
        }

    }

    public bool Insert(CAD231_SOLICITCAO_OCORRENCIA CAD231)
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return cnx.Execute("usp_CAD231_SOLICITACAO_OCORRENCIA_Insert", CAD231, commandType:CommandType.StoredProcedure) > 0;
        }
    }

    public bool Update(int idSolicitacaoOcorrencia, CAD231_SOLICITCAO_OCORRENCIA CAD231)
    {
        return true;
    }

    public bool Delete(int idSolicitacaoOcorrencia)
    {
        return true;
    }
    #endregion

}
