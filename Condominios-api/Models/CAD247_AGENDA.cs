﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
   public class CAD247_AGENDA
    {
        public int IdAgenda { get; set; }
        public int IdLocal { get; set; }
        public int IdProvedor { get; set; }
        public int IdUsuario { get; set; }
        public int IdCliente { get; set; }
        public int IdProduto { get; set; }
        public DateTime DataCadastro { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }

        public List<CAD247_AGENDA> Retrive_Cad100()
        {
            using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
            {
                return (List<CAD247_AGENDA>)cnx.Query<CAD247_AGENDA>("usp_CAD247_AGENDA_List_CAD100", new { @idProvedor = this.IdProvedor }, commandType: CommandType.StoredProcedure);
            }
        }

    }
