﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Web.Http.Cors;

public class Usuario
{

    #region Propriedades
    public int IdUsuario { get; set; }
    public string NomeUsuario { get; set; }
    public string LoginUsuario { get; set; }
    public byte[] SenhaUsuario { get; set; }
    public string Cpf { get; set; }
    public int? IdPerfil { get; set; }
    public int IdStatusUsuario { get; set; }
    public bool IsBloqueadoSistema { get; set; }
    public char TipoAcesso { get; set; }
    public int IdProvedor { get; set; }
    public int? IdCliente { get; set; }
    public string EmailUsuario { get; set; }
    public string SenhaEntradaUSuario { get; set; }
    #endregion

    #region Metodos
    public Usuario Logar(string login, string senha)
    {
        using (var conexaoBD = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            var usuario = conexaoBD.Query<Usuario>("API_usp_Usuario_SelectLoginSenha",
                                            new
                                            {
                                                @LoginUSuario = login,
                                                @SenhaUsuario = senha,
                                                @fraseEncriptedPassword = System.Configuration.ConfigurationManager.AppSettings["FraseEncriptedPasswordBD"]
                                            },
                                                commandType: CommandType.StoredProcedure);
            return usuario.FirstOrDefault();
        }
    }
    public List<Usuario> List()
    {
        using (SqlConnection conexaoBD = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<Usuario>)conexaoBD.Query<Usuario>("Select * from Usuario");
        }
    }
    #endregion
}
