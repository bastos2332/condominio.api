﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

public class CAD228_ARQUIVO
{
    public int IdArquivo { get; set; }
    public int IdSolicitacaoOcorrencia { get; set; }
    public int IdAndamentoOcorrencia { get; set; }
    public string NomeArquivo { get; set; }
    public string Descricao { get; set; }
    public string Tamanho { get; set; }
    public string Extensao { get; set; }
    public DateTime DataUpload { get; set; }
    public string CaminhoArquivo { get; set; }
    public int IdCLiente { get; set; }
    public int IdUsuario { get; set; }

    public List<CAD228_ARQUIVO> List_CAD100(int idProvedor)
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<CAD228_ARQUIVO>)cnx.Query<CAD228_ARQUIVO>("usp_CAD228_ARQUIVO_List_DOCUMENTO_CONDOMINO",
                                        new
                                        {
                                            @idProvedor = idProvedor
                                        },
                                        commandType: System.Data.CommandType.StoredProcedure);
        }
    }
}
