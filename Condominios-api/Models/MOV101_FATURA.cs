﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;

public class MOV101_FATURA
{
    #region PROPS
    public int IdFatura { get; set; }
    public DateTime DataVencimento { get; set; }
    public decimal ValorTotal { get; set; }
    public int IdStatusFatura { get; set; }
    public int IdCliente { get; set; }
    public int IdProvedor { get; set; }
    public string ChaveBoletoWeb { get; set; }
    #endregion

    #region METHODS
    public List<MOV101_FATURA> List_CAD100_CAD200(int idProvedor, int idCliente, bool isProvedorMaster)
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<MOV101_FATURA>)cnx.Query<MOV101_FATURA>("usp_MOV101_FATURA_List_CAD200_CLIENTE",
                                        new
                                        {
                                            @idProvedor = idProvedor,
                                            @idCliente = idCliente,
                                            @isProvedorMaster = isProvedorMaster
                                        },
                                        commandType: System.Data.CommandType.StoredProcedure);
        }
    }
    #endregion
}
