﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using System.Data;

public class LKP221_STATUS_SOLICITACAO_OCORRENCIA
{
    public int IdStatusOcorrencia { get; set; }
    public int IdProvedor { get; set; }
    public string StatusOcorrencia { get; set; }
    public string TipoAcao { get; set; }


    public List<LKP221_STATUS_SOLICITACAO_OCORRENCIA> List()
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<LKP221_STATUS_SOLICITACAO_OCORRENCIA>)cnx.Query<LKP221_STATUS_SOLICITACAO_OCORRENCIA>("usp_LKP221_STATUS_SOLICITACAO_OCORRENCIA_List", commandType: CommandType.StoredProcedure);
        };
    }
}