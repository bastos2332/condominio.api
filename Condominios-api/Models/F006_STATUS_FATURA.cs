﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class F006_STATUS_FATURA
{
    #region PROPS
    public int IdStatusFatura { get; set; }
    public string NomeStatusFatura { get; set; }
    #endregion

    #region METHODS
    public List<F006_STATUS_FATURA> Get()
    {
        using (SqlConnection conexao = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<F006_STATUS_FATURA>)conexao.Query<F006_STATUS_FATURA>("usp_F006_STATUS_FATURA_List", commandType: CommandType.StoredProcedure);
        }
    }
    #endregion
}

