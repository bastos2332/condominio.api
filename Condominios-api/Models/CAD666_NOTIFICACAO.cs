﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using Dapper;
using System.Data;

public class CAD666_NOTIFICACAO
{

    #region PROPS
    public int IdNotificacao { get; set; }
    public int IdProvedor { get; set; }
    public int IdCliente { get; set; }
    public string TextoNotificacao { get; set; }
    public DateTime DataNotificacao { get; set; }
    public string RotaNotificacao { get; set; }
    public bool IsVisualizada { get; set; }
    public bool IsConteudoVisualizado { get; set; }

    #endregion

    #region METHODS
    
    public List<CAD666_NOTIFICACAO> List_CAD200(int idCliente)
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return (List<CAD666_NOTIFICACAO>)cnx.Query<CAD666_NOTIFICACAO>("usp_CAD666_NOTIFICACAO_List_CAD200",
                                            new {@idCliente = idCliente },
                                            commandType: CommandType.StoredProcedure);
        }
    }

    public bool UpdateViewAlertaNotificacao(int idCliente)
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return cnx.Execute("usp_CAD666_NOTIFICACAO_CAD200_Visualizar_Alerta", new {@idCliente = idCliente }, commandType: CommandType.StoredProcedure) > 0;
        }
    }

    public int UpdateViewConteudoNotificacao(int idNotificacao)
    {
        using (SqlConnection cnx = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString()))
        {
            return cnx.Execute("usp_CAD666_NOTIFICACAO_visualizar_conteudo", new {@idNotificacao = idNotificacao }, commandType: CommandType.StoredProcedure);
        }
    }



    #endregion
}
