﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CAD100Controller : ApiController
    {
        public IHttpActionResult Get(int id)
        {
            var CAD100 = new CAD100_PROVEDOR().Get(id);

            if (CAD100.Count == 0)
            {
                return NotFound();
            }else
            {
                return Ok(CAD100);
            }
            
        }
    }
}
