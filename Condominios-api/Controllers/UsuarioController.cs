﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{



    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class UsuarioController : ApiController
    {
        
        [AcceptVerbs("Post")]
        public IHttpActionResult Logar(UsuarioAutenticacao Auth) {
            return Ok(new Usuario().Logar(Auth.Login, Auth.Senha)) ;
        }
    }
}
