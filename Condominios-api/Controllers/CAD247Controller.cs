﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CAD247Controller : ApiController
    {
        public List<CAD247_AGENDA> Get(int idProvedor)
        {
            return new CAD247_AGENDA() { IdProvedor = idProvedor }.Retrive_Cad100();
        }
    }
}
