﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LKP221Controller : ApiController
    {

        public IHttpActionResult Get()
        {
            return Ok(new LKP221_STATUS_SOLICITACAO_OCORRENCIA().List());
        }
    }
}
