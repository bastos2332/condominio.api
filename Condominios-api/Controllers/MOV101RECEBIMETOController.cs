﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MOV101RECEBIMETOController : ApiController
    {
        public List<MOV101_FATURA_RECEBIMENTO> Get(int idCliente)
        {
            return new MOV101_FATURA_RECEBIMENTO() {IdCliente = idCliente}.List();
        }
    }
}
