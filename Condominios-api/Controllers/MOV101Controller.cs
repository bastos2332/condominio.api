﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MOV101Controller : ApiController
    {

        public IHttpActionResult Get(int idProvedor, int idCliente, bool isProvedorMAster)
        {
            return Ok(new MOV101_FATURA().List_CAD100_CAD200(idProvedor, idCliente, isProvedorMAster));
        }
    }
}
