﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CAD231Controller : ApiController
    {
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(new CAD231_SOLICITCAO_OCORRENCIA().Get());
            }
            catch (Exception)
            {

                throw;
            }

        }
        [HttpGet]
        [Route("api/CAD231")]
        public IHttpActionResult Get_CAD200(int idCliente)
        {
            var CAD231 = new CAD231_SOLICITCAO_OCORRENCIA().Get_CAD200(idCliente);
            if (CAD231.Count == 0)
            {
                return NotFound();
            }
            else
            {
                return Ok(CAD231);
            }
        }


        public IHttpActionResult Post([FromBody] CAD231_SOLICITCAO_OCORRENCIA CAD231)
        {
            if (CAD231 != null)
            {
                return Ok(new CAD231_SOLICITCAO_OCORRENCIA().Insert(CAD231));
            } else
            {
                return BadRequest();
            }
            
        }
    }
}
