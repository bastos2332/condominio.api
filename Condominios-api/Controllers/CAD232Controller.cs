﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CAD232Controller : ApiController
    {
        public IHttpActionResult Get(int id)
        {
            return Ok(new CAD232_ANDAMENTO_OCORRENCIA().List_CAd231(id));
        }
    }
}
