﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Condominios_api.Controllers
{
    public class CAD228Controller : ApiController
    {
        public IHttpActionResult Get(int idProvedor)
        {
            return Ok(new CAD228_ARQUIVO().List_CAD100(idProvedor));
        }
    }
}
