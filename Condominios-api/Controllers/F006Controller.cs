﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Condominios_api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class F006Controller : ApiController
    {
        // GET: api/F006
        public List<F006_STATUS_FATURA> Get()
        {
            return new F006_STATUS_FATURA().Get();
        }
       
    }
}
