﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Condominios_api.Controllers
{
    public class CAD666Controller : ApiController
    {
       
        [HttpGet]
        [Route("api/CAD666")]
        public IHttpActionResult Get_CAD200(int idCliente)
        {
            return Ok(new CAD666_NOTIFICACAO().List_CAD200(idCliente));
        }


        [HttpPut]
        [Route("api/CAD666")]
        public IHttpActionResult VisualizarAlertaNotificacao([FromBody]int idCliente)
        {
            return Ok(new CAD666_NOTIFICACAO().UpdateViewAlertaNotificacao(idCliente));
        }

        [HttpPut]
        [Route("api/CAD666/VIEW-CONTEUDO")]
        public IHttpActionResult VisualizarConteudoNotificacao([FromBody]int idNotificacao)
        {
            return Ok(new CAD666_NOTIFICACAO().UpdateViewConteudoNotificacao(idNotificacao));
        }
    }
}
