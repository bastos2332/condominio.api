﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

public class CAD229Controller : ApiController
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public IHttpActionResult Get(int idProvedor)
    {
        try
        {
            return Ok(new CAD229_PROPAGANDA().Get_CAD100(idProvedor));
        }
        catch (Exception)
        {
            throw;
        }
        
    }
}

